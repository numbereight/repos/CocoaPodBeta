//
//  NEDeviceMovement.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension NEDevicePositionState: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEDevicePositionState in
            return NEDevicePosition_stateFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEDevicePosition_reprFromState(self))
    }
}

extension NEDevicePositionOrientation: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEDevicePositionOrientation in
            return NEDevicePosition_orientationFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEDevicePosition_reprFromOrientation(self))
    }
}

extension NEDevicePosition: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.state = NEDevicePositionState(try decCont.decode(String.self, forKey: .state)) ?? .unknown
        self.orientation = NEDevicePositionOrientation(try decCont.decode(String.self, forKey: .orientation)) ?? .unknown
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.state.description, forKey: .state)
        try encCont.encode(self.orientation.description, forKey: .orientation)
    }

    enum CodingKeys: String, CodingKey {
        case state
        case orientation
    }
}

extension NEDevicePosition: Equatable {
    public static func == (inLhs: NEDevicePosition, inRhs: NEDevicePosition) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEDevicePosition_isEqual(&lhs, &rhs)
    }
}
