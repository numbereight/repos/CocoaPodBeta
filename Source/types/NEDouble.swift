//
//  NEDouble.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension NEDouble: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.value = try decCont.decode(Double.self, forKey: .value)
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.value, forKey: .value)
    }

    enum CodingKeys: String, CodingKey {
        case value
    }
}

extension NEDouble: Comparable {
    public static func<(inLhs: NEDouble, inRhs: NEDouble) -> Bool {
        return inLhs.value < inRhs.value
    }
}

extension NEDouble: AdditiveArithmetic {
    public static var zero: NEDouble {
        return NEDouble(value: 0)
    }

    public static func+(inLhs: NEDouble, inRhs: NEDouble) -> NEDouble {
        return NEDouble(value: inLhs.value + inRhs.value)
    }

    public static func-(inLhs: NEDouble, inRhs: NEDouble) -> NEDouble {
        return NEDouble(value: inLhs.value - inRhs.value)
    }

    public static func*(inLhs: NEDouble, inRhs: NEDouble) -> NEDouble {
        return NEDouble(value: inLhs.value * inRhs.value)
    }

    public static func/(inLhs: NEDouble, inRhs: NEDouble) -> NEDouble {
        return NEDouble(value: inLhs.value / inRhs.value)
    }
}

extension NEDouble: Equatable {
    public static func == (inLhs: NEDouble, inRhs: NEDouble) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEDouble_isEqual(&lhs, &rhs)
    }
}
