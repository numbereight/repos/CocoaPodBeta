//
//  NESituation.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension NESituationMajor: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NESituationMajor in
            return NESituation_majorFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NESituation_reprFromMajor(self))
    }
}

extension NESituationMinor: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NESituationMinor in
            return NESituation_minorFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NESituation_reprFromMinor(self))
    }
}

extension NESituation: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.major = NESituationMajor(try decCont.decode(String.self, forKey: .major)) ?? .unknown
        self.minor = NESituationMinor(try decCont.decode(String.self, forKey: .minor)) ?? .unknown
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.major.description, forKey: .major)
        try encCont.encode(self.minor.description, forKey: .minor)
    }

    enum CodingKeys: String, CodingKey {
        case major
        case minor
    }
}

extension NESituation: Equatable {
    public static func == (inLhs: NESituation, inRhs: NESituation) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NESituation_isEqual(&lhs, &rhs)
    }
}
