//
//  NELocationCluster.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension NELocationCluster: Codable {

    public init(from decoder: Decoder) throws {
        self.init()

        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.centroid = try decCont.decode(NELocationCoordinate2D.self, forKey: .centroid)
        self.clusterID = try decCont.decode(Int32.self, forKey: .clusterID)
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)

        try encCont.encode(self.centroid, forKey: .centroid)
        try encCont.encode(self.clusterID, forKey: .clusterID)
    }

    enum CodingKeys: String, CodingKey {
        case centroid
        case clusterID

    }
}

extension NELocationCluster: Equatable {
    public static func == (inLhs: NELocationCluster, inRhs: NELocationCluster) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NELocationCluster_isEqual(&lhs, &rhs)
    }
}
