//
//  NELocation.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension NELocation: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.coordinate = try decCont.decode(NELocationCoordinate2D.self, forKey: .coordinate)
        self.speed = try decCont.decode(NESpeed.self, forKey: .speed)
        if
            let isNil = try? decCont.decodeNil(forKey: .altitude),
            isNil == false {
            self.altitude = try decCont.decode(NEAltitude.self, forKey: .altitude)
        }
        if
            let isNil = try? decCont.decodeNil(forKey: .course),
            isNil == false {
            self.course = try decCont.decode(NEDirection.self, forKey: .course)
        }

        self.floor = try decCont.decode(NEFloor.self, forKey: .floor)

        if
            let isNil = try? decCont.decodeNil(forKey: .horizontalAccuracy),
            isNil == false {
            self.horizontalAccuracy =
                try decCont.decode(NELocationAccuracy.self, forKey: .horizontalAccuracy)
        }
        if
            let isNil = try? decCont.decodeNil(forKey: .verticalAccuracy),
            isNil == false {
            self.verticalAccuracy = try decCont.decode(NELocationAccuracy.self, forKey: .verticalAccuracy)
        }
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)

        try encCont.encode(self.coordinate, forKey: .coordinate)
        try encCont.encode(self.speed, forKey: .speed)
        if self.altitude == kNEAltitudeUnavailable {
            try encCont.encodeNil(forKey: .altitude)
        } else {
            try encCont.encode(self.altitude, forKey: .altitude)
        }
        if self.course == kNEDirectionUnavailable {
            try encCont.encodeNil(forKey: .course)
        } else {
            try encCont.encode(self.course, forKey: .course)
        }
        try encCont.encode(self.floor, forKey: .floor)
        if self.horizontalAccuracy == kNELocationAccuracyUnavailable {
            try encCont.encodeNil(forKey: .horizontalAccuracy)
        } else {
            try encCont.encode(self.horizontalAccuracy, forKey: .horizontalAccuracy)
        }
        if self.verticalAccuracy == kNELocationAccuracyUnavailable {
            try encCont.encodeNil(forKey: .verticalAccuracy)
        } else {
            try encCont.encode(self.verticalAccuracy, forKey: .verticalAccuracy)
        }
    }

    enum CodingKeys: String, CodingKey {
        case coordinate
        case speed
        case altitude
        case course
        case floor
        case horizontalAccuracy
        case verticalAccuracy
    }
}

extension NELocation: Equatable {
    public static func == (inLhs: NELocation, inRhs: NELocation) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NELocation_isEqual(&lhs, &rhs)
    }
}
