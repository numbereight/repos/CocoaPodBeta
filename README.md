This project holds release builds of NESDKiOS for Cocoapods dependency management.

## Maintainers

* [Matthew Paletta](matt@numbereight.ai)
* [Chris Watts](chris@numbereight.ai)
