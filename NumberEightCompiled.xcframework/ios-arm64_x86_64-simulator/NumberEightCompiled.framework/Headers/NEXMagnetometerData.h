//
//  NEXMagnetometerData.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 28/08/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import <CoreMotion/CMMagnetometer.h>

#import "NEXVector3D.h"

NS_ASSUME_NONNULL_BEGIN
@interface NEXMagnetometerData : NEXVector3D

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)initWithCMMagneticField:(CMMagneticField)magneticField
                             confidence:(double)confidence;

@property(nonatomic, readonly) CMMagneticField cmMagneticField;

@end
NS_ASSUME_NONNULL_END
