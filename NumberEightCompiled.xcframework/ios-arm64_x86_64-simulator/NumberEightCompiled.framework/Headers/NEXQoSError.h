//
//  NEXQoSError.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2022-11-10.
//  Copyright © 2022 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NEQoSErrorType.h"

@class NEXQoSLevel;

@interface NEXQoSError : NSObject

/**
 * @return The category type of this QoS error.
 */
@property(nonatomic, readonly) NEQoSErrorType type;

/**
 * @return The requested QoS level that caused the error.
 */
@property(nonatomic, readonly) NEXQoSLevel* requested;

/**
 * @return The QoS limits for the error producer.
 *      Any non-zero amount indicates a limit from the error producer.
 */
@property(nonatomic, readonly) NEXQoSLevel* limits;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)initWithType:(NEQoSErrorType)type requested:(NEXQoSLevel*)requested;

- (instancetype)initWithType:(NEQoSErrorType)type
                   requested:(NEXQoSLevel*)requested
                      limits:(NEXQoSLevel*)limits;

- (instancetype)initWithType:(NEQoSErrorType)type
                        what:(NSString*)what
                   requested:(NEXQoSLevel*)requested;

- (instancetype)initWithType:(NEQoSErrorType)type
                        what:(NSString*)what
                   requested:(NEXQoSLevel*)requested
                      limits:(NEXQoSLevel*)limits;

@end
