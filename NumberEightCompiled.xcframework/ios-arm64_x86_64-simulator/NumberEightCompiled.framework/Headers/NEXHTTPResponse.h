//
//  NEXHTTPResponse.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2022-05-10.
//  Copyright © 2022 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import <Foundation/Foundation.h>

#import "NEXHTTPRequest.h"

@interface NEXHTTPResponse : NSObject

- (instancetype _Nonnull)init;
+ (instancetype _Nonnull)new;

@property(nonnull, readonly) NSString* data;
@property(readonly) int statusCode;
@property(nonnull, readonly) NEXHTTPRequest* request;
@property(nonnull, readonly) NSDictionary<NSString*, NSString*>* headers;

/**
 * @returns true if the code is in the 200-300 range or is 304 Not Modified.
 */
+ (bool)isOk:(int)code;

/**
 * @returns true if the request succeeded or the server returned 304 Not Modified
 */
- (bool)ok;

/**
 * @returns true if the code is any 400 code that implies the request data must
 *          change before trying again or the server is a teapot.
 */
+ (bool)isUnacceptable:(int)code;

/**
 * @returns true if the request was not accepted by the server due to malformed data
 *          or if the server is a teapot.
 */
- (bool)unacceptable;

- (NSUInteger)headerSize;

- (NSUInteger)totalBytes;

@end
