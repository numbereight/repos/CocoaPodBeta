//
//  NEXQoSLevel.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2022-11-01.
//  Copyright © 2022 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NEXQoSError.h"

/**
 * Class used to specify QoS Requirements when subscribing.  This may be explicitly set in a
 * subscription call, or may be inferred through filters.  If no QoSLevel was expliclty passed, and
 * a QoSLevel could not be inferred through filters, a default QoSLevel is used.
 *
 * Subscribers should expect to receive data at a frequency between @p minFrequency and @p
 * maxFrequency. If only one of
 * @p minFrequency or @p maxFrequency is set, they will be the same value.
 * If neither are set, the QoSLevel is set to the default QoS Level, and the sensor will run at its
 * default rate, set by NumberEight developers, and should be appropriate for most users.
 * @see `[NEXNumberEight subscribeToTopic]`
 */
@interface NEXQoSLevel : NSObject

/**
 * The minimum QoS frequency requested by the user.
 * @return A frequency specified as the minimum frequency. If only the maximum was set, the
 * maximum will be returned.
 */
@property(nonatomic, readwrite) double minFrequency;

/**
 * The maximum QoS frequency requested by the user.
 * @return A frequency specified as the maximum frequency.  If only the minimum was set, the
 * minimum will be returned.
 */
@property(nonatomic, readwrite) double maxFrequency;

/**
 * Boolean indicating whether this object is a default QoS level.
 * @return `true` if this is a default QoS level.  Returns `false` if either `minFrequency` or
 * `maxFrequency` have been previously set.
 */
@property(nonatomic, readonly) BOOL isDefault;

/**
 * Constructs a new default QoS level object.
 */
- (instancetype _Nonnull)init;

/**
 * Constructs a new default QoS level object.
 */
+ (instancetype _Nonnull)new;

/**
 * Constructs a new QoS level object, with @p minFrequency and @p maxFrequency set to @p frequency.
 */
- (instancetype _Nonnull)initWithFrequency:(double)frequency;

- (BOOL)isEqual:(id _Nullable)object;

/**
 * Returns a representation of this QoS Level as a filter string.
 * e.g.
 *     frequency:2s
 *     frequency:200ms
 * Used by the Engine to appropriately combine with the other filter strings set by the user.
 * @return Filter string representation of the current QoS level.
 * @see `[NEXNumberEight subscribeToTopic]`
 */
- (NSString* _Nonnull)toFilterStr;

/**
 * Returns a combine QoS level.  If both @p q1 and @p q2 are default, it should return a default
 * QoS level. If one of @p q1 or @p q2 is default, it should return the non-default one. If
 * neither are default, it should return the highest minimum and highest maximum of both.
 * @return combined QoS level between @p q1 and @p q2
 */
+ (NEXQoSLevel* _Nonnull)combineLevelsWithQ1:(NEXQoSLevel* _Nullable)q1
                                          q2:(NEXQoSLevel* _Nullable)q2;

@end
