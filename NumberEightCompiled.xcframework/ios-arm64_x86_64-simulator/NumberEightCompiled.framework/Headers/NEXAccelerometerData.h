//
//  NEXAccelerometerData.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 28/08/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import <CoreMotion/CMAccelerometer.h>

#import "NEXVector3D.h"

NS_ASSUME_NONNULL_BEGIN
@interface NEXAccelerometerData : NEXVector3D

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)initWithCMAcceleration:(NEVector3D)acceleration confidence:(double)confidence;

@property(nonatomic, readonly) CMAcceleration cmAcceleration
    __attribute__((deprecated("use acceleration instead.")));
@property(nonatomic, readonly) NEVector3D acceleration;

@end
NS_ASSUME_NONNULL_END
