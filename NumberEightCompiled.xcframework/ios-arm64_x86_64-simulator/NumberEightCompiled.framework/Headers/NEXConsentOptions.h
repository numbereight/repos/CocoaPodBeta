//
//  NEXConsentOptions.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2020-10-13.
//  Copyright © 2020 NumberEight Technologies Ltd. All rights reserved.
//

#pragma once
#include <Foundation/Foundation.h>

#include "ConsentOptionsProperty.h"
#include "Consentable.h"
#include "IABTCFv2Utils.h"
#include "OnConsentChangedListener.h"

NS_SWIFT_NAME(ConsentOptions)
@interface NEXConsentOptions : NSObject

/**
 * Constructs default `ConsentOptions` object with legit interest options enabled.
 */
+ (instancetype _Nonnull)withDefault;

/**
 * Utility constructor for use in applications that manage their own consent, or do not need to get
 * explicit consent from users.
 */
+ (instancetype _Nonnull)withConsentToAll;

/**
 * Utility constructor to set all consents and legitimate interest categories to false.
 */
+ (instancetype _Nonnull)withConsentToNone;

/// Returns a new ConsentOptions object with all consents enabled.
///
/// Use this if consent is managed by a framework that NumberEight can
/// read from automatically. Supported frameworks:
/// - IAB TCF v2.0
+ (instancetype _Nonnull)useConsentManager;

/**
 NumberEight Vendor ID Constant.
 */
+ (int)VENDOR_ID NS_SWIFT_NAME(VENDOR_ID()) DEPRECATED_MSG_ATTRIBUTE(
    "Vendor ID is deprecated.  Please use [NEXNumberEight IAB_VENDOR_ID] instead.");

/**
 * Produces a string that represents the state of the consent options.
 * If a consent string was used in the creation of this object, it will be included in the
 * serialized form.
 */
- (NSString* _Nonnull)serialize;

- (bool)isEqual:(NEXConsentOptions* _Nullable)other;
- (NSUInteger)hash;

/**
 Checks if the NEXConsentOptions object has the required consent of the `Consentable` passed in.
 Return true if all requirements of `Consentable` are enabled.  This will be re-checked when
 NumberEight.consentOptions is updated and the sensor will be stopped/started appropriately.
 */
- (bool)hasRequiredConsent:(id<Consentable> _Nullable)listener;

- (void)setConsent:(ConsentOptionsProperty)option to:(bool)to;
- (bool)hasConsent:(ConsentOptionsProperty)option;

@end
