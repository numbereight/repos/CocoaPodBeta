//
//  NEXLambdaFormatterTypes.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-10-14.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import <Foundation/Foundation.h>

@class NEXSnapshot;

typedef NSString* _Nullable (^NEXSnapshotFormatterBlock)(NEXSnapshot* _Nonnull state);
