//
//  NELog.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-04-07.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NELog : NSObject

+ (void)msg:(NSString*)tag error:(NSString* _Nullable)msg;
+ (void)msg:(NSString*)tag
       error:(NSString* _Nullable)msg
    metadata:(NSDictionary<NSString*, id>*)metadata;

+ (void)msg:(NSString*)tag warning:(NSString* _Nullable)msg;
+ (void)msg:(NSString*)tag
     warning:(NSString* _Nullable)msg
    metadata:(NSDictionary<NSString*, id>*)metadata;

+ (void)msg:(NSString*)tag info:(NSString* _Nullable)msg;
+ (void)msg:(NSString*)tag
        info:(NSString* _Nullable)msg
    metadata:(NSDictionary<NSString*, id>*)metadata;

+ (void)msg:(NSString*)tag debug:(NSString* _Nullable)msg;
+ (void)msg:(NSString*)tag
       debug:(NSString* _Nullable)msg
    metadata:(NSDictionary<NSString*, id>*)metadata;

+ (void)msg:(NSString*)tag verbose:(NSString* _Nullable)msg;
+ (void)msg:(NSString*)tag
     verbose:(NSString* _Nullable)msg
    metadata:(NSDictionary<NSString*, id>*)metadata;

@end

NS_ASSUME_NONNULL_END
