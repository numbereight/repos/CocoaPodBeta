//
//  NEXApiToken.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-12-17.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#pragma once

#import <Foundation/Foundation.h>

#if TARGET_OS_IOS
#import <UIKit/UIKit.h>
#endif

#include "NEXConsentOptions.h"

NS_SWIFT_NAME(APIToken)
@interface NEXAPIToken : NSObject

- (instancetype _Nonnull)init NS_UNAVAILABLE;
+ (instancetype _Nonnull)new NS_UNAVAILABLE;

@property(nonatomic, readonly, nonnull) NSString* key;
#if TARGET_OS_IOS
@property(nonatomic, readonly, nullable)
    NSDictionary<UIApplicationLaunchOptionsKey, id>* launchOptions;
#endif

@property(nonatomic, readonly, nonnull) NEXConsentOptions* consentOptions;

@end
