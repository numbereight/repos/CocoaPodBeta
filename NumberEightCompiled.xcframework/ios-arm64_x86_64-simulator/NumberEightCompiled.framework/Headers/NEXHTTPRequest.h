//
//  NEXHTTPRequest.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2022-05-10.
//  Copyright © 2022 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import <Foundation/Foundation.h>

#include "NEHTTPCachePolicy.h"
#include "NEHTTPMethod.h"

@interface NEXHTTPRequest : NSObject

- (instancetype _Nonnull)init;
+ (instancetype _Nonnull)new;
+ (instancetype _Nonnull)newNERequestWithMethod:(enum NEHTTPMethod)method;

@property(nonnull, readwrite) NSString* host;
@property(nonnull, readwrite) NSString* path;
@property(nonnull, readwrite) NSString* data;
@property(readwrite) enum NEHTTPMethod method;
@property(readwrite) enum NEHTTPCachePolicy policy;

- (NSString* _Nonnull)url;
- (NSString* _Nonnull)methodStr;
- (void)setHeaderWithKey:(NSString* _Nonnull)key value:(NSString* _Nonnull)value;

- (bool)isMutating;
+ (NSString* _Nonnull)stringWithCachePolicy:(enum NEHTTPCachePolicy)cachePolicy;
+ (NSString* _Nonnull)stringWithMethod:(enum NEHTTPMethod)method;

@property(nonnull, readonly) NSString* description;
@property(nonnull, readonly) NSDictionary<NSString*, NSString*>* headers;

- (NSUInteger)headerSize;
- (NSUInteger)totalBytes;

@end
