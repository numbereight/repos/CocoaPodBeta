#ifndef NEHTTPCachePolicy_H
#define NEHTTPCachePolicy_H

// Use one on iOS, use the other within the core.
#if __has_include("NETypeUtils.h")
#include "NETypeUtils.h"
#else
#include "../types/ctypes/NETypeUtils.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

NE_ENUM(uint32_t, NEHTTPCachePolicy){
    // based off: https://developer.apple.com/documentation/foundation/nsurlrequestcachepolicy

    // Use the caching logic defined in the protocol implementation, if any, for a particular
    // URL load request.
    DefaultCachePolicy,

    // The URL load should be loaded only from the originating source.
    ReloadIgnoringLocalCacheData,

    // Ignore local cache data, and instruct proxies and other intermediates to disregard their
    // caches so far as the protocol allows.
    ReloadIgnoringLocalandRemoteCacheData,

    // Use existing cache data, regardless or age or expiration date, loading from originating
    // source only if there is no cached data.
    ReturnCacheDataElseLoad,

    // Use existing cache data, regardless or age or expiration date, and fail if no cached
    // data is available.
    ReturnCacheDataDontLoad,

    // Use cache data if the origin source can validate it; otherwise, load from the origin.
    ReloadRevalidatingCacheData,
};

#ifdef __cplusplus
}
#endif

#endif /* NEHTTPCachePolicy_H */
