/**
 * @file NEVolumeCategory.h
 * NEVolumeCategory type.
 */

#ifndef NEVolumeCategory_H
#define NEVolumeCategory_H

#include <stdbool.h>

#include "NETypeUtils.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Possible states for NEVolumeCategory.
 */
NE_ENUM(uint32_t, NEVolumeCategoryState){
    NEVolumeCategoryStateUnknown = 0, NEVolumeCategoryStateOff,  NEVolumeCategoryStateLow,
    NEVolumeCategoryStateMid,         NEVolumeCategoryStateHigh, NEVolumeCategoryStateMax,
};

/**
 * Represents the current volume level split into categories.
 */
typedef struct NEVolumeCategory {
    /**
     * Represents the current volume level split into categories.
     */
    NEVolumeCategoryState state;
} NEVolumeCategory;

/**
 * Default NEVolumeCategory instance.
 */
static const NEVolumeCategory NEVolumeCategory_default = { .state = NEVolumeCategoryStateUnknown };

/**
 * C String array mapping for NEVolumeCategoryState
 */
static const char* const NEVolumeCategoryStrings[] = {
    [NEVolumeCategoryStateUnknown] = "Unknown", [NEVolumeCategoryStateOff] = "Off",
    [NEVolumeCategoryStateLow] = "Low",         [NEVolumeCategoryStateMid] = "Mid",
    [NEVolumeCategoryStateHigh] = "High",       [NEVolumeCategoryStateMax] = "Max",
};

static const char* const NEVolumeCategoryStateReprs[] = {
    [NEVolumeCategoryStateUnknown] = "unknown", [NEVolumeCategoryStateOff] = "off",
    [NEVolumeCategoryStateLow] = "low",         [NEVolumeCategoryStateMid] = "mid",
    [NEVolumeCategoryStateHigh] = "high",       [NEVolumeCategoryStateMax] = "max",
};

const char* NEVolumeCategory_stringFromState(NEVolumeCategoryState state);
const char* NEVolumeCategory_reprFromState(NEVolumeCategoryState state);
NEVolumeCategoryState NEVolumeCategory_stateFromRepr(const char* repr);

/**
 * Returns true if the all the fields are identical in the two objects, false otherwise.
 *
 * @param lhsPtr A pointer to an NEVolumeCategory struct to compare context in it.
 * @param rhsPtr A pointer to an NEVolumeCategory struct to compare against.
 */
bool NEVolumeCategory_isEqual(
    const NEVolumeCategory* const lhsPtr,
    const NEVolumeCategory* const rhsPtr);

#ifdef __cplusplus
}
#endif

#endif /* NEVolumeCategory_H */
