//
//  NEXEngine+internal.h
//  NumberEightCompiled
//
//  Created by Matt on 2021-08-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#pragma once
#include <necore/engine/Engine.hpp>

#include "NEXEngine.h"

@interface NEXEngine (internal)

@property(readonly, atomic) bool isRunning;
@property(readonly, atomic) bool isStarted;

@property(readonly, nonatomic) std::shared_ptr<NECore::Engine> engineImpl;

- (void)registerSensor:(NEXSensor* _Nonnull)sensor;

@end
