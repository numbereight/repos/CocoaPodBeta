/**
 * @file NEString.h
 * NEString type.
 */

#ifndef NEString_H
#define NEString_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Encapsulates a string
 */
typedef struct NEString {
    char* value;
} NEString;

/**
 * Default NEString instance.
 */
static const NEString NEString_default = { .value = 0 };

/**
 * Returns true if the all the fields are identical in the two objects, false otherwise.
 *
 * @param lhsPtr A pointer to an NEBoolean struct to compare context in it.
 * @param rhsPtr A pointer to an NEBoolean struct to compare against.
 */
bool NEString_isEqual(const NEString* const lhsPtr, const NEString* const rhsPtr);

#ifdef __cplusplus
}
#endif

#endif /* NEString_H */
