//
//  NEXSnapshotPair.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-10-14.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//
#import <Foundation/Foundation.h>

NS_SWIFT_NAME(SnapshotPair)
@interface NEXSnapshotPair : NSObject

@property(readwrite, strong, atomic) NSString* _Nullable first;
@property(readwrite, strong, atomic) id _Nullable second;

- (instancetype _Nonnull)init NS_UNAVAILABLE;
- (instancetype _Nonnull)new NS_UNAVAILABLE;

- (instancetype _Nonnull)initWithKey:(NSString* _Nullable)key value:(id _Nullable)value;

@end
