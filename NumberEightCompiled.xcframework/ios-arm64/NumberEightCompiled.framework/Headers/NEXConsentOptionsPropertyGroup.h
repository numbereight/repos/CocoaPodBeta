//
//  NEXConsentOptionsPropertyGroup.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2022-11-17.
//  Copyright © 2022 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ConsentOptionsProperty.h"

@interface NEXConsentOptionsPropertyGroup : NSObject

@property(nonatomic, readonly) NSUInteger count;

- (BOOL)hasConsentProperty:(ConsentOptionsProperty)property;
- (instancetype _Nonnull)addConsentProperty:(ConsentOptionsProperty)property;
- (instancetype _Nonnull)removeConsentProperty:(ConsentOptionsProperty)property;

@end
