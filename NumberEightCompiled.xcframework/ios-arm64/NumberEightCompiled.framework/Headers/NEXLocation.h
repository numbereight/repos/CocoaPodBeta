//
//  NEXLocation.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 19/06/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import <CoreLocation/CoreLocation.h>

#import "NELocation.h"
#import "NEXSensorItem.h"
#import "NEXVector3D.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * Represents a latitude and longitude in decimal degrees, and a speed in metres per second.
 */
@interface NEXLocation : NEXSensorItem

/**
 * Default constructor.
 */
- (instancetype)init;

/**
 * Default constructor.
 */
+ (instancetype)new;

/**
 * Conversion copy constructor.
 * @param location NELocation to copy from.
 */
- (instancetype)initWithLocation:(NELocation)location;

/**
 * Conversion copy constructor.
 * @param location NELocation to copy from.
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithLocation:(NELocation)location confidence:(double)confidence;

/**
 * @param coordinate NELocationCoordinate2D in decimal degrees.
 * @param speed Instantaneous speed in metres per second.
 * @param altitude The altitude in metres.
 * @param course The instantaneous course in degrees from True North.
 * @param floor The floor number, where 0 means ground floor.
 * @param horizontalAccuracy The radius of uncertainty of the coordinates, in metres.
 * @param verticalAccuracy The amount of uncertainty of the altitude, in metres.
 */
- (instancetype)initWithCoordinate:(NELocationCoordinate2D)coordinate
                             speed:(NESpeed)speed
                          altitide:(NEAltitude)altitude
                            course:(NEDirection)course
                             floor:(NEFloor)floor
                horizontalAccuracy:(NELocationAccuracy)horizontalAccuracy
                  verticalAccuracy:(NELocationAccuracy)verticalAccuracy;

/**
 * @param coordinate NELocationCoordinate2D in decimal degrees.
 * @param speed Instantaneous speed in metres per second.
 * @param altitude The altitude in metres.
 * @param course The instantaneous course in degrees from True North.
 * @param floor The floor number, where 0 means ground floor.
 * @param horizontalAccuracy The radius of uncertainty of the coordinates, in metres.
 * @param verticalAccuracy The amount of uncertainty of the altitude, in metres.
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithCoordinate:(NELocationCoordinate2D)coordinate
                             speed:(NESpeed)speed
                          altitide:(NEAltitude)altitude
                            course:(NEDirection)course
                             floor:(NEFloor)floor
                horizontalAccuracy:(NELocationAccuracy)horizontalAccuracy
                  verticalAccuracy:(NELocationAccuracy)verticalAccuracy
                        confidence:(double)confidence;

/**
 * @param coordinate NELocationCoordiante2D in decimal degrees.
 */
- (instancetype)initWithCoordinate:(NELocationCoordinate2D)coordinate;

/**
 * @param coordinate NELocationCoordiante2D in decimal degrees.
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithCoordinate:(NELocationCoordinate2D)coordinate confidence:(double)confidence;

/**
 * @param coordinate NELocationCoordiante2D in decimal degrees.
 * @param speed Instantaneous speed in metres per second.
 */
- (instancetype)initWithCoordinate:(NELocationCoordinate2D)coordinate speed:(NESpeed)speed;

/**
 * @param coordinate NELocationCoordiante2D in decimal degrees.
 * @param speed Instantaneous speed in metres per second.
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithCoordinate:(NELocationCoordinate2D)coordinate
                             speed:(NESpeed)speed
                        confidence:(double)confidence;

/**
 * @param latitude Latitude in decimal degrees.
 * @param longitude Longitude in decimal degrees.
 */
- (instancetype)initWithLatitude:(double)latitude longitude:(double)longitude;

/**
 * @param latitude Latitude in decimal degrees.
 * @param longitude Longitude in decimal degrees.
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithLatitude:(double)latitude
                       longitude:(double)longitude
                      confidence:(double)confidence;

/**
 * @param latitude Latitude in decimal degrees.
 * @param longitude Longitude in decimal degrees.
 * @param speed Instantaneous speed in metres per second.
 */
- (instancetype)initWithLatitude:(double)latitude longitude:(double)longitude speed:(NESpeed)speed;

/**
 * @param latitude Latitude in decimal degrees.
 * @param longitude Longitude in decimal degrees.
 * @param speed Instantaneous speed in metres per second.
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithLatitude:(double)latitude
                       longitude:(double)longitude
                           speed:(NESpeed)speed
                      confidence:(double)confidence;

/**
 * @param latitude Latitude in decimal degrees.
 * @param longitude Longitude in decimal degrees.
 * @param speed Instantaneous speed in metres per second.
 * @param altitude The altitude in metres.
 * @param course The instantaneous course in degrees from True North.
 * @param floor The floor number, where 0 means ground floor.
 * @param horizontalAccuracy The radius of uncertainty of the coordinates, in metres.
 * @param verticalAccuracy The amount of uncertainty of the altitude, in metres.
 */
- (instancetype)initWithLatitude:(double)latitude
                       longitude:(double)longitude
                           speed:(NESpeed)speed
                        altitude:(NEAltitude)altitude
                          course:(NEDirection)course
                           floor:(NEFloor)floor
              horizontalAccuracy:(NELocationAccuracy)horizontalAccuracy
                verticalAccuracy:(NELocationAccuracy)verticalAccuracy;

/**
 * @param latitude Latitude in decimal degrees.
 * @param longitude Longitude in decimal degrees.
 * @param speed Instantaneous speed in metres per second.
 * @param altitude The altitude in metres.
 * @param course The instantaneous course in degrees from True North.
 * @param floor The floor number, where 0 means ground floor.
 * @param horizontalAccuracy The radius of uncertainty of the coordinates, in metres.
 * @param verticalAccuracy The amount of uncertainty of the altitude, in metres.
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithLatitude:(double)latitude
                       longitude:(double)longitude
                           speed:(NESpeed)speed
                        altitude:(NEAltitude)altitude
                          course:(NEDirection)course
                           floor:(NEFloor)floor
              horizontalAccuracy:(NELocationAccuracy)horizontalAccuracy
                verticalAccuracy:(NELocationAccuracy)verticalAccuracy
                      confidence:(double)confidence;

/**
 * Distance calculation using a locally-flat Earth approximation
 * Accurate over short distances
 */
- (double)flatEarthDistanceToLocation:(NEXLocation*)location;

/**
 * Parse a string in serialized form.
 * @param string The serialized string, as generated by serialize()
 */
+ (instancetype _Nullable)unserializeWithString:(NSString*)string;

/**
 * Gets the displacement in metres between two Locations as a 2D vector.
 * Uses flat space approximation, so only works for small displacements (i.e. less than 300 metres).
 * The third component will always be 0.
 *
 * @see flatEarthDistanceToLocation
 */
+ (NEXVector3D* _Nullable)getDisplacementVectorWithLocation:(NEXLocation* _Nullable)location1
                                                  location2:(NEXLocation* _Nullable)location2;

/**
 * The 2D coordinate encapsulating a latitude and longitude in decimal degrees.
 */
@property(nonatomic, readwrite) NELocationCoordinate2D coordinate;

/**
 * The speed in metres per second, if speed is unknown then this value defaults to
 * kNESpeedUnavailable.
 */
@property(nonatomic, readwrite) NESpeed speed;

/**
 * The altitude in meters, if altitude is unknown then this value defaults to
 * kNEAltitudeUnavailable.
 */
@property(nonatomic, readwrite) NEAltitude altitude;

/**
 *  The course of the location in degrees true North. Negative if course is invalid.
 *  If course is unknown then this value defaults to kNEDirectionUnavailable.
 */
@property(nonatomic, readwrite) NEDirection course;

/**
 * The  floor level, 0 means ground floor, if floor is unknown then this value defaults to
 * kNEFloorUnavailable.
 */
@property(nonatomic, readwrite) NEFloor floor;

/**
 * The horizontal accuracy of the location in meters. The radius of the circle the location is
 * within. Negative if the lateral location is invalid. If unknown then this value defaults to
 * kNELocationAccuracyUnavailable.
 */
@property(nonatomic, readwrite) NELocationAccuracy horizontalAccuracy;

/**
 * The vertical accuracy of the location in meters. Negative if the altitude is invalid. If unknown
 * then this value defaults to kNELocationAccuracyUnavailable.
 */
@property(nonatomic, readwrite) NELocationAccuracy verticalAccuracy;

+ (NELocation)neLocationFromCLLocation:(nullable CLLocation*)clLocation;
+ (NELocationCoordinate2D)neCoordinateFromCLCoordiante:(CLLocationCoordinate2D)clCoordinate;
+ (CLLocationCoordinate2D)clCoordinateFromNECoordiante:(NELocationCoordinate2D)neCoordinate;

@property(nonatomic, readonly) NELocation value DEPRECATED_ATTRIBUTE;

/**
 * @return A hash value representing the state of the Type.
 *          Can be used for checking equality.
 */
@property(nonatomic, readonly) NSUInteger hash;

/**
 * @return The name of the concrete value class extending NEXSensorItem.
 */
@property(nonatomic, readonly) NSString* type;

/**
 * @return A human-readable format of the held data.
 */
@property(nonatomic, readonly) NSString* description;

/**
 * @return A machine-readable format of the held data.  Returns `nil` if @p type is invalid, or if
 * serialization failed.
 */
- (NSString* _Nullable)serialize;

/**
 * Strict equality comparison.
 *
 * @see NELocation_isEqual
 *
 * @param object Value to compare with.
 */
- (BOOL)isEqual:(id _Nullable)object;

/**
 * Convert the Objective-C class to a C-compatible struct.
 */
- (NELocation)c_val;

@end
NS_ASSUME_NONNULL_END
