//
//  NEXLocationClusterID.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 28/08/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import "NEInteger.h"
#import "NEXSensorItem.h"

NS_ASSUME_NONNULL_BEGIN
@interface NEXLocationClusterID : NEXSensorItem

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)initWithClusterID:(NEInteger)clusterID confidence:(double)confidence;

@property(nonatomic, readonly) int32_t clusterID;

@end
NS_ASSUME_NONNULL_END
